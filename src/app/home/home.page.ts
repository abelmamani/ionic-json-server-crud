import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule, ModalController } from '@ionic/angular';
import { Product } from '../models/product.model';
import { ProductService } from '../services/product.service';
import { ModalUpdateProductComponent } from './components/modal-update-product/modal-update-product.component';
import { ModalCreateProductComponent } from './components/modal-create-product/modal-create-product.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule]
})
export class HomePage implements OnInit {
  products: Product[] = [];
  isAlertOpen: boolean = false;
  isToastOpen: boolean = false;
  message: string ="";
  constructor(private productService: ProductService, private modalCtrl: ModalController) { }
  id: number = 2;
  ngOnInit() {
    this.getAllCategories();
  }
  getAllCategories(){
    this.productService.getProducts().subscribe({
      next: (response: Product[]) => {
      this.products = response;
    }, 
    error: (error) => {
      this.showToast("Hubo un problema al traer el listado de productos!");
    }});
  }

  async openCreateModal() {
    const modal = await this.modalCtrl.create({
      component: ModalCreateProductComponent,
    });
    modal.present();
    const {  role } = await modal.onWillDismiss();
    if (role === 'confirm'){
      this.showToast("Se registro un nuevo producto.");
      this.getAllCategories();
    }
  }

  async openUpdateModal(id: number) {
    const modal = await this.modalCtrl.create({
      component: ModalUpdateProductComponent,componentProps: {idProduct: id }});
    modal.present();
    const { role } = await modal.onWillDismiss();
    if (role === 'confirm') {
      this.showToast("Se actulizo el producto correctamente.");
      this.getAllCategories();
    }
  }

  deleteProduct(id: number){
      this.setOpenAlert(true);
      this.id = id;
  }

  setOpenToast(isOpen: boolean) {
    this.isToastOpen = isOpen;
  }
  setOpenAlert(isOpen: boolean) {
    this.isAlertOpen = isOpen;
  }
  showToast(msg: string){
    this.message = msg;
    this.setOpenToast(true);
  }

  public alertButtons = [
    {
      text: 'No',
      role: 'cancel',
      handler: () => {
        this.showToast("Accion cancelada!");
      },
    },
    {
      text: 'Si',
      role: 'confirm',
      handler: () => {
        this.productService.deleteProduct(this.id).subscribe({
          next: (res) => {
            this.showToast("Se ha eliminado el producto.");
            this.getAllCategories();
          },
          error: (err) =>{
            this.showToast(err.error ? err.error.message : "Hubo un problema al eliminar el producto!");
          }});
      }
    },
  ];
}

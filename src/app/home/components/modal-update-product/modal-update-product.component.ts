import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { IonicModule, ModalController } from '@ionic/angular';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-modal-update-product',
  templateUrl: './modal-update-product.component.html',
  styleUrls: ['./modal-update-product.component.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, ReactiveFormsModule]
})
export class ModalUpdateProductComponent  implements OnInit {
  @Input() idProduct!: number;
  isToastOpen: boolean = false;
  message: string="";
  formProduct: FormGroup;
  
  constructor(private modalCtrl: ModalController, private productServie: ProductService, private formBuilder: FormBuilder) {
    this.formProduct = this.formBuilder.group({
      id: 0,
      name: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(45)]],
      description: ['', [Validators.required, Validators.maxLength(200)]],
      price: [null, [Validators.required, Validators.min(1)]],
      stock: [null, [Validators.required, Validators.min(1)]],
      createAt: [new Date()]
    });
    }
  
  ngOnInit(): void {
    this.productServie.getProductById(this.idProduct).subscribe({
      next: (res: Product) =>{
        this.formProduct.patchValue(res);
      }, 
      error: (err) => {
        this.message = err.error ? err.error.message : "Ocurrio un error al obtner el producto.";
        this.setOpen(true);
      }});
  }

  formSubmit(){
    if(this.formProduct.valid){
      this.productServie.updateProduct(this.idProduct, this.formProduct.value).subscribe({
        next: (res) =>{
          this.confirm();
        },
        error: (err)=> {
          this.message = err.error ? err.error.message : "Ocurrio un error al actualizar.";
        this.setOpen(true);
      }});
    }}

  confirm(){
    return this.modalCtrl.dismiss(null, 'confirm');
  }

  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }
  setOpen(state: boolean){
    this.isToastOpen = state;
  }
}

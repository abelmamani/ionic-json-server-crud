import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { IonicModule, ModalController } from '@ionic/angular';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-modal-create-product',
  templateUrl: './modal-create-product.component.html',
  styleUrls: ['./modal-create-product.component.scss'],
  standalone: true,
  imports: [IonicModule,CommonModule, ReactiveFormsModule]
})
export class ModalCreateProductComponent{
  formProduct: FormGroup;
  isToastOpen: boolean = false;
  message: string = "";

  constructor(private modalCtrl: ModalController, private productServie: ProductService, private formBuilder: FormBuilder) {
    this.formProduct = this.formBuilder.group({
      id: 0,
      name: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(45)]],
      description: ['', [Validators.required, Validators.maxLength(200)]],
      price: [null, [Validators.required, Validators.min(1)]],
      stock: [null, [Validators.required, Validators.min(1)]],
      createAt: [new Date()]
    });
    }
  
  formSubmit(){
    if(this.formProduct.valid){
      this.productServie.addProduct(this.formProduct.value).subscribe({
        next: (res) =>{
          this.confirm();
        },
        error: (err)=> {
          this.message = err.error ? err.error.message : "Ocurrio un error al registrar.";
          this.setOpen(true);
        }});
    }
  }
  confirm() {
      return this.modalCtrl.dismiss(null, 'confirm');
  }
  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }
  setOpen(state: boolean){
    this.isToastOpen = state;
  }

}
